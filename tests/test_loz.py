from loz import loz
from loz.exceptions import *

import json
import pytest
import random
import shutil
import string

### Fixtures

PASSWORD = "n4NO39C39jqAPVp9RSOID6oEyNeh2rVJ"
LOZFILE = "lozfile"
CSV_FILE = "lozfile.csv"


@pytest.fixture()
def lozfile():
    suffix = "".join(random.choice(string.ascii_lowercase) for _ in range(16))
    new_lozfile = f"lozfile_{suffix}.test_artifact"
    shutil.copyfile(LOZFILE, new_lozfile)
    return new_lozfile


@pytest.fixture()
def lozfile_bad_version():
    suffix = "".join(random.choice(string.ascii_lowercase) for _ in range(16))
    new_lozfile = f"lozfile_{suffix}.test_artifact"
    with open(LOZFILE, "r") as f:
        storage = json.loads(f.read())
    storage["version"] = "2.0"
    with open(new_lozfile, "w") as f:
        f.write(json.dumps(storage))
    return new_lozfile


@pytest.fixture
def storage():
    with open(LOZFILE, "r") as f:
        s = json.loads(f.read())
    return s


@pytest.fixture
def storage_small(storage):
    del storage["data"]["two.user.domain"]["tom"]
    del storage["data"]["single.user.domain"]
    storage["data"]["multi.line.domain"]["multi@line.user"] = "changed value"
    return storage


def storage_contains_storage(storage1, storage2):
    for domain in storage2["data"]:
        assert domain in storage1["data"]
        for user in storage2["data"][domain]:
            assert user in storage1["data"][domain]


@pytest.fixture
def csv_file():
    return CSV_FILE


@pytest.fixture
def csv_data(csv_file):
    with open(csv_file, "r") as f:
        return f.read()


def test_loz_load_filepath(lozfile, lozfile_bad_version):
    storage = loz.load(lozfile)
    assert storage
    with pytest.raises(LozFileDoesNotExist) as e:
        storage = loz.load("does_not_exist")
    with pytest.raises(LozIncompatibleFileVersion) as e:
        storage = loz.load(lozfile_bad_version)


def test_loz_save(storage, lozfile):
    loz.save(storage, lozfile)
    with open(lozfile, "r") as f:
        new_storage = json.loads(f.read())
    assert storage == new_storage


def test_loz_validate_key(storage):
    assert loz.validate_key(storage, PASSWORD)
    assert not loz.validate_key(storage, "badkey")


def test_loz_init():
    storage = loz.init(PASSWORD)
    assert loz.validate_key(storage, PASSWORD)


def test_loz_export_csv(storage, csv_data):
    assert loz.validate_key(storage, PASSWORD)
    csv = loz.export_csv(storage)
    assert csv == csv_data


def test_loz_import_csv(storage, storage_small, csv_file):
    with open(csv_file, "r") as f:
        changes = list(loz.import_csv(storage_small, f))
    assert len(changes) == 6
    for change in changes:
        if change[1] == "single.user.domain":
            assert change[0] == "+"
        elif change[1] == "two.user.domain" and change[2] == "tom":
            assert change[0] == "+"
        else:
            assert change[0] == "-+"
    storage_contains_storage(storage_small, storage)


def test_loz_change_password(storage):
    assert loz.validate_key(storage, PASSWORD)
    new_pass = "7a6we5bf76ae5"
    new_storage = loz.change_password(storage, new_pass)
    storage_contains_storage(new_storage, storage)
    storage_contains_storage(storage, new_storage)
    assert loz.validate_key(new_storage, new_pass)
    assert not loz.validate_key(new_storage, PASSWORD)


def test_loz_store(storage):
    # add new domain
    change = loz.store(storage, "new.domain", "new_user", "new_secret")
    assert change[0] == "+"
    secret = loz.get(storage, "new.domain", "new_user")
    assert secret == "new_secret"
    # update user
    change = loz.store(storage, "new.domain", "new_user", "updated_secret")
    assert change[0] == "-+"
    secret = loz.get(storage, "new.domain", "new_user")
    assert secret == "updated_secret"
    # new user in existing domain
    change = loz.store(storage, "new.domain", "another_user", "new_secret")
    assert change[0] == "+"
    secret = loz.get(storage, "new.domain", "another_user")
    assert secret == "new_secret"


def test_loz_make(storage):
    assert loz.validate_key(storage, PASSWORD)
    change = loz.make(storage, "new.domain", "new_user")
    assert change[0] == "+"
    assert "new_user" in storage["data"]["new.domain"]


def test_loz_exists(storage):
    assert not loz.exists(storage, "new_domain")
    assert not loz.exists(storage, "new_domain", "new_user")
    assert loz.exists(storage, "two.user.domain")
    assert not loz.exists(storage, "two.user.domain", "new_user")
    assert loz.exists(storage, "two.user.domain", "tom")


def test_loz_get(storage):
    assert loz.validate_key(storage, PASSWORD)
    assert not loz.get(storage, "bad.domain", "bad_user")
    assert not loz.get(storage, "two.user.domain", "bad_user")
    assert loz.get(storage, "two.user.domain", "tom") == "tom pass"


def test_loz_get_all(storage):
    assert loz.validate_key(storage, PASSWORD)
    data = list(loz.get_all(storage))
    data_dict = {key: [] for key, value, _ in data}
    for key, value, _ in data:
        data_dict[key].append(value)
    storage_contains_storage({"data": data_dict}, storage)
    storage_contains_storage(storage, {"data": data_dict})


def test_loz_rm(storage):
    # delete non-existent user
    change = loz.rm(storage, "two.user.domain", "fake_user")
    assert change[0] == "|"
    # delete non-existent domain
    change = loz.rm(storage, "non.existent.domain")
    assert change[0] == "|"
    # delete user
    assert loz.exists(storage, "two.user.domain", "tom")
    change = loz.rm(storage, "two.user.domain", "tom")
    assert change[0] == "-"
    assert not loz.exists(storage, "two.user.domain", "tom")
    assert loz.exists(storage, "two.user.domain", "jerry")
    assert loz.exists(storage, "two.user.domain")
    # delete last user in domain
    change = loz.rm(storage, "two.user.domain", "jerry")
    assert change[0] == "-"
    assert not loz.exists(storage, "two.user.domain", "jerry")
    assert not loz.exists(storage, "two.user.domain")
    # delete domain
    assert loz.exists(storage, "single.user.domain")
    change = loz.rm(storage, "single.user.domain")
    assert change[0] == "-"
    assert not loz.exists(storage, "single.user.domain")


def test_loz_show(storage):
    assert loz.validate_key(storage, PASSWORD)
    data = list(loz.show(storage, "two.user.domain"))
    assert len(data) == 2
    assert data[0] == ["two.user.domain", "tom", "tom pass"]
    assert data[1] == ["two.user.domain", "jerry", "jerry pass"]
    data = list(loz.show(storage, "single.user.domain"))
    assert len(data) == 1
    assert data[0] == ["single.user.domain", "forever@alone.com", "alone pass"]
    data = list(loz.show(storage, "fake.domain"))
    assert not data


def test_loz_ls(storage):
    data = loz.ls(storage)
    assert data == [
        "two.user.domain",
        "single.user.domain",
        "multi.line.domain",
        "!@#$%^&*()_-+=|\\][{}/?;:'\"<>,.",
        "huge.secret.domain",
    ]
    data = loz.ls(storage, "two.user.domain")
    assert data == ["tom", "jerry"]
    data = loz.ls(storage, "fake.domain")
    assert not data


def test_loz_find(storage):
    data = loz.find(storage, "domain")
    assert data == [
        ["two.user.domain"],
        ["single.user.domain"],
        ["multi.line.domain"],
        ["huge.secret.domain"],
    ]
    data = loz.find(storage, "user")
    assert data == [
        ["two.user.domain"],
        ["single.user.domain"],
        ["multi.line.domain", "multi@line.user"],
        ["huge.secret.domain", "paranoid@user.com"],
    ]
    data = loz.find(storage, "fake")
    assert not data
