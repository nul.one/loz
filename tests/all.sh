#!/usr/bin/env bash
# -*- coding: UTF-8 -*-

set -e
script_dir=$(cd `dirname "$0"`; pwd; cd - 2>&1 >> /dev/null)
cd $script_dir

pip install -U $script_dir/..
pip install wheel black pytest pytest-cov coverage

black $script_dir/../loz --check
black $script_dir --check
py.test -vv --cov=loz --cov-report html
coverage report

exit 0

